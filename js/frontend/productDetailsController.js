app.controller('productDetailsController' , function($scope , $http , $sce , $cookies , $rootScope){

   var product_url_id = window.location.href.split('=')[1];
   var token = "Token "+$cookies.get('user_autheticate_details');

   $scope.product_attrib = {} ;
  
   $scope.current_url = window.location.href ;

  //alert($scope.current_url);


   var cookie_user_id = $cookies.get('user_id');


   var product_id = product_url_id ;
   if(product_url_id.indexOf("#") == -1){

        product_id = product_url_id ;

   }else{

       product_id = product_url_id.split("#")[0];

   }

//----------------- GETTING PRODUCT ATTRIBUTE ----- //

/*
$scope.productAttribute = function(){

  $http({
       method: 'get',

     url: url+'/api/ProductAttribute/'+product_id,

   }).then(function(response){

  //   alert(response.data);
     console.log(response.data);
   });
}

$scope.productAttribute(); */



//------------ END HERE ------------------------------//
$scope.getProductImage = function(){
 var data = {PageSize:10};
  $http({
       method: 'post',
     data: data,
     url: url+'/api/Products/'+product_id+'/ProductPictureList',

   }).then(function(response){


          $scope.product_picture = response.data.Data ;


   });







}

 $scope.getProductImage();

 $scope.increseCounter = function(event){

    var productCount = $("#orderQuantity").val();



    var newNumber = parseInt(productCount)+1 ;

    $("#orderQuantity").val(newNumber);


 }

 $scope.decreseCounter = function(event){

    var productCount = $("#orderQuantity").val();

    if(productCount == 0){

       return false;
    }

    var newNumber = parseInt(productCount)-1 ;

    $("#orderQuantity").val(newNumber);


 }

$scope.submitReview = function(){


var data =  {


    "AddProductReview": {
      "Title": $scope.review_title,
      "ReviewText": $("#new-review").val(),
      "Rating": 5 - $(".glyphicon-star-empty").length,
      "CanCurrentCustomerLeaveReview": true,
      "SuccessfullyAdded": true,
      "Result": "sample string 6"
    }
  }

  $http({
       method: 'post',
       data: data,
     url: url+'/api/Products/Reviews/'+product_url_id+'/CreateReviews',
     headers: {
      "Content-Type": "application/json",
      "Accept": 'application/json',
      'Authorization': token
      }
   }).then(function (response){

            $scope.review_message = "Thank You , For Review this product. Your Review is valuable" ;
            $scope.review_title = '';
            $("#new-review").val('');
            $scope.loadReview();

   }).catch(function(){
   
      
        if(response.data === "Customer cannot add more than 3 reviews for a product"){

            $scope.review_message = response.data ;
        }
        else{

             window.location.href = "/signin.html";
        }
        
   
   });



}





   $scope.loadProductDetails = function(){




        $http({
             method: 'get',
           url: url+'/api/Products/'+product_url_id,

         }).then(function (response){
            
              console.log(response.data.ProductAttributes[0]);

              response.data.html_short_description = $sce.trustAsHtml(response.data.ShortDescription);
              response.data.html_full_description = $sce.trustAsHtml(response.data.FullDescription);
               $scope.product_details = response.data  ;

               $scope.setAttribObj(response.data.ProductAttributes[0].Values[0] , response.data.Id);







                //  var review_data = {productId:product_url_id};

          });


   };


   $scope.review_data  = [] ;

   var PageCounter = 1 ;

   var PageSize = 5 ;
   
   $scope.rating_div = [] ;


   $scope.loadReview = function(review_id){


     var data = {Page:PageCounter , PageSize:PageSize};



     if(review_id == "load_more"){
     if(PageSize == 10){

      PageCounter += 1 ;

      PageSize = 5 ;

    }
    else{

      $scope.review_data  = [] ;
      PageSize += 5 ;

      

    }
  }












       $http({
          method: 'post',
          data: data,
          url: url+'/api/Products/Reviews/CustomerProductReviews/'+product_id,
          headers: {
           "Content-Type": "application/json",
           "Accept": 'application/json',
           'Authorization': token
           }

        }).then(function (response){
             console.log(response.data);
             $scope.total_review = response.data.Data.length ;

             if(typeof review_id == "undefined" || review_id == "load_more"){

              $scope.review_data = response.data.Data ;

             }else{

               $scope.review_data = [];

               for(var i = 0 ; i< response.data.Data.length ; i++){

                 //------- Do Computations Here -------- //

                    if(response.data.Data[i].Rating == review_id){

                      $scope.review_data.push(response.data.Data[i]);

                    }


                 //------- End Here ----------------------//



               }



             }


             var sum = 0 ;
             var ficount = 0 ;
             var focount = 0 ;
             var thcount = 0 ;
             var twcount = 0 ;
             var ocount = 0 ;
             $scope.rating_count = {one_star:0 , two_star:0 , three_star:0 , four_star:0 , five_star: 0};

             for(var i = 0  ; i<response.data.Data.length ; i++){

                 sum = sum + response.data.Data[i].Rating;

                 //----- GET ALL COUNT ------ //



                  if(response.data.Data[i].Rating == 5){

                        ficount = ficount + 1 ;
                        $scope.rating_count.five_star = ficount ;

                  }

                  if(response.data.Data[i].Rating == 4){
                       focount = focount + 1 ;
                       $scope.rating_count.four_star = focount ;

                  }

                  if(response.data.Data[i].Rating == 3){
                       thcount = thcount + 1 ;
                       $scope.rating_count.three_star = thcount ;

                  }
                  if(response.data.Data[i].Rating == 2){

                    twcount = twcount + 1 ;
                    $scope.rating_count.two_star = twcount ;

                  }
                  if(response.data.Data[i].Rating == 1){
                      ocount = ocount + 1 ;
                      $scope.rating_count.one_star  = ocount

                  }

               }

	 

            if(response.data.Data.length > 0){
            $scope.avg_rating = sum / response.data.Data.length ;
          }else{
             $scope.avg_rating = 0 ;
          }


	$scope.int_avg_rating = parseInt($scope.avg_rating);
	
	
	$scope.rating_div = [] ;
	
	
	for(var i = 0 ; i < 5 ; i++){
	  
	   if(i < $scope.int_avg_rating){
	   
	      $scope.rating_div.push({index:i , img:'ystr.png'});
	   
	   }else{
	   
	      $scope.rating_div.push({index:i , img: 'gstr.png'});
	   
	   }
	
	}
	
	console.log($scope.rating_div)
	




         }).catch(function(data){

             $scope.total_review = 0 ;
             $scope.avg_rating = 0 ;

         });




   };

   $scope.loadReview();

   $scope.addToList = function(type , event){
         
          let product_det = $(event.target).attr('data-set');

          //alert(product_det);
          
          let parseJson = JSON.parse(product_det);

          console.log(parseJson.ProductAttributes);
           var orderQuantity = parseInt($("#orderQuantity").val());
           //var shoppingCartTypeId = $cookies.get('user_id') ;
           var productId = product_id ;

           if(type == "cart"){
           var shoppingCartTypeId = 1 ;
           }
           if(type == "wishlist"){

            var shoppingCartTypeId = 2 ;
           }
           if(type == "Sample"){

             var shoppingCartTypeId = 3 ;
           }

          // var data = {productId:productId , shoppingCartTypeId:shoppingCartTypeId , quantity:orderQuantity};



       /*    $http({
          method: 'post',
          data: data ,
          url: url+'/api/ShoppingCart/AddProductToCart?productId='+productId+'&shoppingCartTypeId='+shoppingCartTypeId+'&quantity='+orderQuantity,
          headers: {
           "Content-Type": "application/json",
           "Accept": 'application/json',
           'Authorization': token
           }
        }).then(function (response){

             console.log('result',response.data);



            // alert("I am here");
             if(response.data.success == true){

                  $scope.message = $sce.trustAsHtml(response.data.message+" <a href='closeMe'>CloseMe</a>") ;
                  $rootScope.$emit("GetCustomerReport", {});

             }else if(response.data.success == false){

                 $scope.message = $sce.trustAsHtml(response.data.message[0]+" <a href='closeMe'>CloseMe</a>") ;
             }


        }).catch(function(){


             window.location.href = "signin.html" ;

        }); */

    
      let productKey = "addtocart_"+productId+".EnteredQuantity" ;
      let product_attribute_id = "product_attribute_"+parseJson.ProductAttributes[0].ProductId ;

      //let opt = {} ;
     
      //opt[product_attribute_id] = parseJson.ProductAttributes[0].Values[0].Id ;
      $scope.product_attrib[productKey] = orderQuantity ;
      

     // let data = JSON.stringify(opt) ;




        $http({
          method: 'post',
          data: $scope.product_attrib ,
          transformRequest: function(obj) {
            var str = [];
            for(var p in obj)
            str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
            return str.join("&");
        },
          url: url+'/api/ShoppingCart/AddProductToCart_Details?productId='+productId+'&shoppingCartTypeId='+shoppingCartTypeId ,
          headers: {
           "Content-Type": "application/x-www-form-urlencoded",
           "Accept": 'application/json',
           'Authorization': token
           }
        }).then(function (response){

          if(response.data.success == true){
                              
                              if(shoppingCartTypeId === 1){
                                $scope.message = $sce.trustAsHtml("<p>Product has successfully added to cart");
                              }else if(shoppingCartTypeId === 2){
                                $scope.message = $sce.trustAsHtml("<p>Product has successfully added to wishlist");

                              }else{

                                $scope.message = $sce.trustAsHtml("<p>Product has successfully added to sample list");
                              }

                              
                             
                              $rootScope.$emit("GetCustomerReport", {});
            
                         }else if(response.data.success == false){
            
                             $scope.message = $sce.trustAsHtml(response.data.message[0]) ;
                         }


        }).catch(function(){


          window.location.href = "signin.html" ;

        });









   };

   $scope.show = "0" ;
   $scope.setAttribObj = function(obj , id){

    //alert(id) ;
    //let productKey = "addtocart_"+productId+".EnteredQuantity" ;
   
    let product_attribute_id = "product_attribute_"+id ;

    $scope.product_attrib[product_attribute_id] = obj.Id ;

    $scope.selectedMessage  = "You have selected "+obj.Name ;
    
    $scope.newvalue  = $scope.product_details.ProductPrice.PriceValue  + obj.PriceAdjustmentValue ; 

    $scope.show = "1" ;
     


   }

   $scope.removeItem = function(){

    $scope.show = "0" ;
    $scope.selectedMessage = "";
    $scope.product_attrib = {} ;
   }







});
