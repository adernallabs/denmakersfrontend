app.controller('productResultController', function ($scope, $http, $cookies, $rootScope, $sce, $filter) {

  var current_url_id = window.location.href.split('=')[1];
  var array_products = [];



  $scope.product_search_data = $cookies.get('search_product');







  var token = "Token " + $cookies.get('user_autheticate_details');




  //----------------- LOADING THE CATEGORY --------- //

  $scope.createClass = function (class_name) {
    // alert(class_name);
    $scope.class_name = class_name;


  }


  $scope.loadCategory = function () {

    var data = {
      PageSize: 100
    };

    $http({
      method: 'post',
      data: data,
      url: url + '/api/Categories/',
    }).then(function (response) {

      // alert(response.data);
      //console.log(response.data);
      $scope.product_categories = response.data.Data;



    });

  }

  $scope.loadCategory();


  var countPageNumber = 1;

  var numberOfPages = 10;


  //---------------- END HERE -------------------------//





  $scope.loadProducts = function (val, offset) {
    $scope.is_loading = 0;
    if (typeof val == "undefined" && typeof current_url_id != "undefined") {

      val = current_url_id;
    }

    if (offset == true) {
      countPageNumber = 1;
      $scope.product_search_data = "";
      $scope.loading_text = "Loading Products ...";

    } else {

      $scope.loading_text = "Removing Products ...";
    }
    if (val != "" && $scope.product_search_data == "") {

      var data = {
        "SearchCategoryId": val,
        Page: countPageNumber,
        PageSize: numberOfPages
      };
    } else if ($scope.product_search_data != "") {

      var data = {
        "SearchProductName": $scope.product_search_data,
        Page: countPageNumber,
        PageSize: numberOfPages
      };

    } else {

      var data = {
        Page: countPageNumber,
        PageSize: numberOfPages
      };
    }
     countPageNumber = countPageNumber + 1;

 //   console.log(data);
    $http({
      method: 'post',
      data: data,
      url: url + '/api/Products',
    }).then(function (response) {
      $scope.totalProducts = response.data.Total;
      for (var i = 0; i < response.data.Data.length; i++) {

        if (offset == true || offset == undefined) {
          array_products.push({
            id: val,
            product: response.data.Data[i]
          });

        }
      }



      if (offset == false) {


        array_products = array_products.filter(function (obj) {
          return obj.id !== val;
        });



      }


      $scope.numberOfProducts = array_products.length + " Products Found";
      $scope.current_url_id = current_url_id;
      $scope.product_data = array_products;
      $scope.is_loading = 1;
      $rootScope.$emit("setSearchValue", {
        val: $scope.product_search_data
      });


      console.log($scope.product_data.length);


    });

  };


  $scope.get_category_product = function (val, event) {

    //----- Sending Post Response Here  ------------//
    var checkIfChecked = $(event.target).is(':checked');

    $scope.getAllManufactuerer(val);
    $scope.loadProducts(val, checkIfChecked);

  }






  $scope.addToList = function (val) {

    var orderQuantity = 1;
    //var shoppingCartTypeId = $cookies.get('user_id') ;
    var productId = val;
    var shoppingCartTypeId = 1;





    var data = {
      productId: productId,
      shoppingCartTypeId: shoppingCartTypeId,
      quantity: orderQuantity
    };


    $http({
      method: 'post',
      data: data,
      url: url + '/api/ShoppingCart/AddProductToCart?productId=' + productId + '&shoppingCartTypeId=' + shoppingCartTypeId + '&quantity=' + orderQuantity,
      headers: {
        "Content-Type": "application/json",
        "Accept": 'application/json',
        'Authorization': token
      }
    }).then(function (response) {





      //console.log(response.data);



      if (response.data.success == true) {

        $scope.message = $sce.trustAsHtml(response.data.message);
        $rootScope.$emit("GetCustomerReport", {});

      } else if (response.data.success == false) {

        $scope.message = $sce.trustAsHtml(response.data.message);
      }

      $("#success_modal").modal('show');

    }).catch(function (response) {


      window.location.href = "signin.html";

    });







  };





  $scope.getAllManufactuerer = function (getid) {


    $http.get(url + '/api/Categories/' + getid + '/manufacturers').then(function (response) {



      console.log(response.data);
      //$scope.allBrands = "Suspense";
      $scope.allBrands = response.data.data;

    }).catch(function (response) {
      console.log(response.data);
    });

    //alert(getid);




  };




});


//-------------------- END HERE ---------------------------//