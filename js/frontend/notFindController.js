app.controller('notFindController' , function($scope , $http , $cookies){
      var token = "Token "+$cookies.get('user_autheticate_details');


      //==============get all cities========================

      $http.get(url+'/api/cities/').then(function(response){
          $scope.citylist = response.data.Data;
      }).catch(function(resp){
        console.log('err',resp)
          //alert("State No Error Occured");

      });

    //==============end all cities=========================
    //===============get all states============================
           $http.get(url+'/api/states/').then(function(response){
               $scope.statelist = response.data.Data;

           }).catch(function(resp){
             console.log('err',resp)
               //alert("State No Error Occured");

           });

    //==========end all states================================


    $scope.submitForm = function(){

    /*   var residential = $scope.residential ;
       var sqft = $scope.sqft ;
       var budget = $scope.budget ;
       var scope_of_work = $scope.scope_of_work ;
       var name = $scope.name ;
       var email_address = $scope.email_address ;
       var phone_number = $scope.phone_number ;
       var alternate_phone = $scope.alternate_phone ;
       var city = $scope.city ;
       var pincode = $scope.pincode ;
       var comment = $scope.comment ; */


      var title = "Contact Request";


      if($scope.name == "" || $scope.name == undefined){

          $scope.validation_message = "Name is Required";
          return false;
      }

      if($scope.email_address == "" || $scope.email_address == undefined){

          $scope.validation_message = "Email is Required";
          return false;
      }

      if($scope.phone_number == "" || $scope.phone_number == undefined){

          $scope.validation_message = "Phone Number is Required";
          return false;
      }

      if($scope.city == "" || $scope.city == undefined){

          $scope.validation_message = "City is Required";
          return false;
      }
      if($scope.state == "" || $scope.state == undefined){

          $scope.validation_message = "State is Required";
          return false;
      }
      if($scope.pincode == "" || $scope.pincode == undefined){

          $scope.validation_message = "Pincode is Required";
          return false;
      }



 $scope.comment="";


       var html = `<div style="font-family: Helvetica Neue, Helvetica, Helvetica, Arial, sans-serif;">
    <table style="width: 100%;">
      <tr>
        <td></td>
        <td bgcolor="#FFFFFF ">
          <div style="padding: 15px; max-width: 600px;margin: 0 auto;display: block; border-radius: 0px;padding: 0px; border: 1px solid lightseagreen;">
            <table style="width: 100%;background: #d5a705 ;">
              <tr>
                <td></td>
                <td>
                  <div>
                    <table width="100%">
                      <tr>
                        <td rowspan="2" style="text-align:center;padding:10px;">
							<img style="float:left; width: 160px;"  src="https://ourtestserver.website/dreamsale/images/logo.png" />

							<span style="color:white;float:right;font-size: 13px;font-style: italic;margin-top: 20px; padding:10px; font-size: 14px; font-weight:normal;">
							"One stop solution , for all your interior and real estate needs."<span></span></span></td>
                      </tr>
                    </table>
                  </div>
                </td>
                <td></td>
              </tr>
            </table>
            <table style="padding: 10px;font-size:14px; width:100%;">
              <tr>
                <td style="padding:10px;font-size:14px; width:100%;">
                <h1 style="color:#d74113; margin-bottom:30px; width:100%; display:block; text-align:center">`+title+`</h1>


                    <p style="width:100%; margin:0; padding:8px 0px; display:block; border-bottom:1px dashed #999; font-size:16px;" ><strong style="margin-right:10px; font-size:20px;">Name :</strong>   `+$scope.name+` </p>
                    <p style="width:100%; margin:0; padding:8px 0px; display:block; border-bottom:1px dashed #999; font-size:16px;" ><strong style="margin-right:10px; font-size:20px;">Phone Number :</strong>   `+$scope.phone_number+` </p>

                    <p style="width:100%; margin:0; padding:8px 0px; display:block; border-bottom:1px dashed #999; font-size:16px;" ><strong style="margin-right:10px; font-size:20px;">City :</strong>   `+$scope.city+` </p>
                   <p style="width:100%; margin:0; padding:8px 0px; display:block; border-bottom:1px dashed #999; font-size:16px;" ><strong style="margin-right:10px; font-size:20px;">State :</strong>   `+$scope.state+` </p>
                    <p style="width:100%; margin:0; padding:8px 0px; display:block; border-bottom:1px dashed #999; font-size:16px;" ><strong style="margin-right:10px; font-size:20px;">Pincode :</strong>   `+$scope.pincode+` </p>
                    <p style="width:100%; margin:0; padding:8px 0px; display:block; border-bottom:1px dashed #999; font-size:16px;" ><strong style="margin-right:10px; font-size:20px;">Comment :</strong>   `+$scope.comment+` </p>
                    <p style="width:100%; margin:0; padding:8px 0px; display:block; border-bottom:1px dashed #999; font-size:16px;" >
                    Thank you for writing to us .Our team shall contact you with 24 hours . We regret to hear that you could not find what you were looking for.`<br> +`Regards`+`  </p>
                 </td>
              </tr>
			  <tr>
			  <td>
				 <div align="center" style="font-size:12px; margin-top:20px; padding:5px; width:100%; background:#eee;">
                    © 2018 <a href="" target="_blank" style="color:#333; text-decoration: none;">Denmakers.com</a>
                  </div>
                </td>
			  </tr>
            </table>
          </div>` ;



         var data = {Subject: title , body:html, To:$scope.email_address};
         console.log(data);
          $http({
            method: 'post',
            data: data,
            url: url+'/api/EmailAccounts/SendEmail'

          }).then(function (response){

                 $scope.message = "Thank you for contacting us. We will get back to you soon.";
                 $("#status_model").modal('show');


          });





    }

});
