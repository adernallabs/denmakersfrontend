app.controller('headerController', function ($scope, $http, $cookies, $rootScope) {

  document.onclick = function (event) {
    // Compensate for IE<9's non-standard event model
    //
    $('.child').hide();
    $('.nchild').hide();

  };
  $scope.logged_in_token = $cookies.get('user_autheticate_details');
  $scope.logged_in_email = $cookies.get('logged_in_email');




  $scope.total_cart_length = 0;


  $scope.redirect = function (url) {

    window.location.href = url;
  }

  $rootScope.$on("GetCustomerReport", function () {

    //  $scope.getCustomerDetails();


  });

  $rootScope.$on("setSearchValue", function (args) {
    var cookie_details = $cookies.get('search_product');
    $scope.searchDetails = cookie_details;
    $cookies.put('search_product', '');

  });





  //---- GETTING DATA OF PARTICULAR PRODUCTS ------ //

  $scope.getProducts = function (val) {


    $cookies.put('search_product', val);

    window.location.href = 'search_result.html?category_id=';


  }




  //------------- END HERE ----------------------------//






  /*

    $scope.getCustomerDetails = function(){
       var token = "Token "+$scope.logged_in_token ;


       var data = {"SearchEmail":$scope.logged_in_email};




        var config = {
          headers: {
            'Authorization': token,
            'Content-Type': 'application/json',
            'Accept': 'application/json'
          }
        };


        $http({
       method: 'post',
       data: data ,
       url: url+'/api/customers/',
       headers: {
        "Content-Type": "application/json",
        "Accept": 'application/json',
        'Authorization': token
        }
     }).then(function (response){

       //    alert(response.data.Data[0].Id);
           $cookies.put('user_id', response.data.Data[0].Id);





     });







   }  */



  $scope.cartDetails = function () {

    var token = "Token " + $scope.logged_in_token;


    if (typeof $scope.logged_in_token != "undefined") {

      $http({
        method: 'get',

        url: url + '/api/ShoppingCart/GetCartDetailsLoggedinUser/1/',
        headers: {
          "Content-Type": "application/json",
          "Accept": 'application/json',
          'Authorization': token
        }
      }).then(function (response) {

        // alert(response.data);
        $scope.total_cart_length = response.data.Items.length;
        $scope.cart_item = response.data.Items;






      });

    }



  }

  $scope.cartDetails();




  $rootScope.$emit("GetCustomerReport", {});
  $scope.do_logout = function () {

    console.log(document.cookie);
    console.log($cookies.getAll());

    for (key in $cookies.getAll()) {

      console.log(key);
      $cookies.remove(key);

    }





    // $cookies.remove('logged_in_email');
    // $cookies.remove('user_autheticate_details');




    //console.log(gapi.load('auth2'));






    window.location.href = "signin.html";


  };





  $scope.loadCategory = function () {

    var data = {
      PageSize: 100
    };

    let category_array = [];

    $http({
      method: 'post',
      data: data,
      url: url + '/api/Categories/',
    }).then(function (response) {

      // alert(response.data);
      //console.log(response.data);
      console.log(response.data);



      $scope.product_categories = response.data.Data;



    });

  }

  $scope.loadCategory();





  $scope.get_category_product = function (val) {




    window.location.href = "search_result.html?category_id=" + val;

  }

  $scope.redirect = function (val) {

    window.location.href = val;


  }


  $scope.showSubmenu = function (event, type) {
    event.stopPropagation();
    $('.child').hide();

    if (typeof type === "undefined") {

      $('.nchild').hide();

    }





    let className = $(event.target).next().toggle();



  }
  $('html').click(function () {

    //Hide the menus if visible
    $("#product_menu").css('display','none');
  });

  $scope.showService = function (event) {
    $(event.target).next().toggle();
  }
});
document.onkeydown = function (evt) {
  evt = evt || window.event;
  if (evt.keyCode == 27) {
    $("#status_model").modal('hide');
  }
};