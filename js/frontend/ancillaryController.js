app.controller('ancillaryController' , function($scope , $http , $cookies){
      var token = "Token "+$cookies.get('user_autheticate_details');


//==============get all cities========================

  $http.get(url+'/api/cities/').then(function(response){
      $scope.citylist = response.data.Data;
  }).catch(function(resp){
    console.log('err',resp)
      //alert("State No Error Occured");

  });

//==============end all cities=========================
//===============get all states============================
       $http.get(url+'/api/states/').then(function(response){
           $scope.statelist = response.data.Data;

       }).catch(function(resp){
         console.log('err',resp)
           //alert("State No Error Occured");

       });

//==========end all states================================

      $scope.submitForm = function(){

     var title = "Ancillary Services";

     console.log($scope.date);
     if(typeof $scope.service_name == "undefined" || $scope.service_name == ""){

          $scope.validation_message = "Service Name is required";
          return false;
     }
    //  if(typeof $scope.date == "undefined" || $scope.date == ""){

    //   $scope.validation_message = "Date is Required";
    //   return false;


    // }
    // if(typeof $scope.time_slot == "undefined" || $scope.time_slot == ""){
    //   console.log($scope.time_slot);
    //   $scope.validation_message = "Time is Required";
    //   return false;


    // }
     if(typeof $scope.name == "undefined" || $scope.name == ""){

        $scope.validation_message = "Name is Required";
        return false;

     }
     if(typeof $scope.email_address == "undefined" || $scope.email_address == ""){

        $scope.validation_message = "Email is Required";
        return false;

     }
     if(typeof $scope.phone_number == "undefined" || $scope.phone_number == ""){

       $scope.validation_message = "Phone Number is Required";
       return false;


     }
     if(typeof $scope.address == "undefined" || $scope.address == ""){

       $scope.validation_message = "Address is Required";
       return false;


     }
     if(typeof $scope.city == "undefined" || $scope.city == ""){

       $scope.validation_message = "City is Required";
       return false;


     }
     if(typeof $scope.state == "undefined" || $scope.state == ""){

       $scope.validation_message = "State is Required";
       return false;


     }
     if(typeof $scope.pincode == "undefined" || $scope.pincode == "" ||$scope.pincode.length!=6){

       $scope.validation_message = "Pincode should be 6 digits long";
       return false;


     }
     if(typeof $scope.comment == "undefined" || $scope.comment == ""){

      $scope.comment="None";


    }






      var html = `<div style="font-family: Helvetica Neue, Helvetica, Helvetica, Arial, sans-serif;">
   <table style="width: 100%;">
     <tr>
       <td></td>
       <td bgcolor="#FFFFFF ">
         <div style="padding: 15px; max-width: 600px;margin: 0 auto;display: block; border-radius: 0px;padding: 0px; border: 1px solid lightseagreen;">
           <table style="width: 100%;background: #d5a705 ;">
             <tr>
               <td></td>
               <td>
                 <div>
                   <table width="100%">
                     <tr>
                       <td rowspan="2" style="text-align:center;padding:10px;">
             <img style="float:left; width: 160px;"  src="https://ourtestserver.website/dreamsale/images/logo.png" />

             <span style="color:white;float:right;font-size: 13px;font-style: italic;margin-top: 20px; padding:10px; font-size: 14px; font-weight:normal;">
             "One stop solution , for all your interior and real estate needs."<span></span></span></td>
                     </tr>
                   </table>
                 </div>
               </td>
               <td></td>
             </tr>
           </table>
           <table style="padding: 10px;font-size:14px; width:100%;">
             <tr>
               <td style="padding:10px;font-size:14px; width:100%;">
               <h1 style="color:#d74113; margin-bottom:30px; width:100%; display:block; text-align:center">`+title+`</h1>

                   <p style=" width:100%; margin:0; padding:8px 0px; display:block; border-bottom:1px dashed #999; font-size:16px; "><strong style="margin-right:10px; font-size:20px;">Service :</strong> `+$scope.service_name+`</p>
                   <p style="width:100%; margin:0; padding:8px 0px; display:block; border-bottom:1px dashed #999; font-size:16px; "> <strong style="margin-right:10px; font-size:20px;">Date:</strong>  `+$scope.date+`</p>
                   <p style="width:100%; margin:0; padding:8px 0px; display:block; border-bottom:1px dashed #999; font-size:16px;" ><strong style="margin-right:10px; font-size:20px;">Time Slot :</strong>   `+$scope.time_slot+` </p>
                   <p style="width:100%; margin:0; padding:8px 0px; display:block; border-bottom:1px dashed #999; font-size:16px;" ><strong style="margin-right:10px; font-size:20px;">Name :</strong>   `+$scope.name+` </p>
                   <p style="width:100%; margin:0; padding:8px 0px; display:block; border-bottom:1px dashed #999; font-size:16px;" ><strong style="margin-right:10px; font-size:20px;">Phone Number :</strong>   `+$scope.phone_number+` </p>
                   <p style="width:100%; margin:0; padding:8px 0px; display:block; border-bottom:1px dashed #999; font-size:16px;" ><strong style="margin-right:10px; font-size:20px;">Address :</strong>   `+$scope.address+` </p>
                   <p style="width:100%; margin:0; padding:8px 0px; display:block; border-bottom:1px dashed #999; font-size:16px;" ><strong style="margin-right:10px; font-size:20px;">City :</strong>   `+$scope.city+` </p>
                  <p style="width:100%; margin:0; padding:8px 0px; display:block; border-bottom:1px dashed #999; font-size:16px;" ><strong style="margin-right:10px; font-size:20px;">State :</strong>   `+$scope.state+` </p>
                   <p style="width:100%; margin:0; padding:8px 0px; display:block; border-bottom:1px dashed #999; font-size:16px;" ><strong style="margin-right:10px; font-size:20px;">Pincode :</strong>   `+$scope.pincode+` </p>
                   <p style="width:100%; margin:0; padding:8px 0px; display:block; border-bottom:1px dashed #999; font-size:16px;" ><strong style="margin-right:10px; font-size:20px;">Comment :</strong>   `+$scope.comment+` </p>
                </td>
             </tr>
       <tr>
       <td>
        <div align="center" style="font-size:12px; margin-top:20px; padding:5px; width:100%; background:#eee;">
                   © 2018 <a href="" target="_blank" style="color:#333; text-decoration: none;">Denmakers.com</a>
                 </div>
               </td>
       </tr>
           </table>
         </div>` ;



        var data = {Subject: title , body:html, To:$scope.email_address};
         $http({
           method: 'post',
           data: data,
           url: url+'/api/EmailAccounts/SendEmail'

         }).then(function (response){


               if(response.status == 200){
                   $scope.message = "Thank you for contacting us. We will get back to you soon.";
                   $("#status_model").modal('show');
               }


         });





   }

});
