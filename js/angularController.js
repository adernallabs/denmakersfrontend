var app = angular.module('dreamsaleApp',['ngCookies']);

var url = "https://denmakers-001-site1.itempurl.com";


 app.controller('headerController', function($scope , $http , $cookies, $rootScope) {

   $scope.logged_in_token = $cookies.get('user_autheticate_details');
   $scope.logged_in_email = $cookies.get('logged_in_email');


   $scope.total_cart_length = 0 ;


  $scope.redirect = function(url){

    window.location.href = url ;
  }

  $rootScope.$on("GetCustomerReport", function(){

               $scope.getCustomerDetails();


  });

  $rootScope.$on("setSearchValue", function(args){
        var cookie_details = $cookies.get('search_product');
        $scope.searchDetails = cookie_details ;
        $cookies.put('search_product' , '');

  });





  //---- GETTING DATA OF PARTICULAR PRODUCTS ------ //

$scope.getProducts = function(val){


    $cookies.put('search_product', val);

    window.location.href = 'search_result.html?category_id=' ;


}




//------------- END HERE ----------------------------//



  $scope.cartData = function(){



        $("#cartModel").modal('show');
        $scope.getCustomerDetails();




  };




   $scope.getCustomerDetails = function(){
      var token = "Token "+$scope.logged_in_token ;


      var data = {"SearchEmail":$scope.logged_in_email};




       var config = {
         headers: {
           'Authorization': token,
           'Content-Type': 'application/json',
           'Accept': 'application/json'
         }
       };


       $http({
      method: 'post',
      data: data ,
      url: url+'/api/customers/',
      headers: {
       "Content-Type": "application/json",
       "Accept": 'application/json',
       'Authorization': token
       }
    }).then(function (response){

      //    alert(response.data.Data[0].Id);
          $cookies.put('user_id', response.data.Data[0].Id);





    });



    $scope.cartDetails = function(){

      $http({
         method: 'get',

         url: url+'/api/ShoppingCart/GetCartDetailsLoggedinUser/1/',
         headers: {
          "Content-Type": "application/json",
          "Accept": 'application/json',
          'Authorization': token
          }
       }).then(function (response){

                // alert(response.data);
                 $scope.total_cart_length = response.data.Items.length ;
                 $scope.cart_item = response.data.Items ;






        });





    }

    $scope.cartDetails();



  }

 $rootScope.$emit("GetCustomerReport", {});
  $scope.do_logout = function(){


    $cookies.remove('logged_in_email');
    $cookies.remove('user_autheticate_details');



    window.location.href = "/dreamsale/signin.html";


  };





   $scope.loadCategory = function(){

      var data = {PageSize:100};

         $http({
        method: 'post',
        data: data ,
        url: url+'/api/Categories/',
     }).then(function (response){

            // alert(response.data);
			//console.log(response.data);
			$scope.product_categories = response.data.Data ;



     });

   }

   $scope.loadCategory();





     $scope.get_category_product = function(val){




      window.location.href = "search_result.html?category_id="+val ;

}

$scope.redirect = function(val){

   window.location.href = val ;


}




});












//------ GETTING PRODUCT LISTING PAGE ----------------------//
app.controller('productResultController', function($scope , $http , $cookies , $rootScope , $sce){

   var current_url_id = window.location.href.split('=')[1] ;
   var array_products = [];



  $scope.product_search_data = $cookies.get('search_product');







   var token = "Token "+$cookies.get('user_autheticate_details');




//----------------- LOADING THE CATEGORY --------- //




$scope.loadCategory = function(){

  var data = {PageSize:100};

      $http({
     method: 'post',
     data : data ,
     url: url+'/api/Categories/',
  }).then(function (response){

         // alert(response.data);
   //console.log(response.data);
   $scope.product_categories = response.data.Data ;



  });

}

$scope.loadCategory();


var countPageNumber = 1 ;

var numberOfPages = 10 ;


//---------------- END HERE -------------------------//





$scope.loadProducts = function(val, offset){
  $scope.is_loading = 0 ;
   if(typeof val == "undefined" && typeof current_url_id != "undefined"){

        val = current_url_id ;
 }


 /*if(typeof offset == "undefined"){

      offset = true ;
 } */




 if(offset == true){
     countPageNumber = 1 ;
    $scope.product_search_data = "";
    $scope.loading_text = "Loading Products ..." ;

 }
 else{

    $scope.loading_text = "Removing Products ...";
 }







   if(val != "" && $scope.product_search_data == ""){

       var data = {"SearchCategoryId": val , Page:countPageNumber , PageSize:numberOfPages};


   }

   else if($scope.product_search_data != ""){

         var data = {"SearchProductName": $scope.product_search_data , Page:countPageNumber , PageSize:numberOfPages};


   }
   else{

    var data = {Page:countPageNumber , PageSize:numberOfPages};
    //   var data = {};


   }


     countPageNumber = countPageNumber + 1;

 console.log(data);
  $http({
       method: 'post',
       data : data,
       url: url+'/api/Products',
   }).then(function (response){



            for(var i = 0 ; i < response.data.Data.length ; i++){

              if(offset == true || offset == undefined){
                   array_products.push({id: val , product: response.data.Data[i]});

               }
               else{

                 array_products = array_products.filter(function( obj ) {
                    return obj.id !== val;
                 });

               }

               }


            $scope.numberOfProducts = array_products.length+" Products Found" ;
            $scope.current_url_id = current_url_id ;
            $scope.product_data = array_products;
            $scope.is_loading = 1 ;
            $rootScope.$emit("setSearchValue", {val:$scope.product_search_data});





    });

  };


  $scope.get_category_product = function(val, event){

     //----- Sending Post Response Here  ------------//
     var checkIfChecked = $(event.target).is(':checked');


      $scope.loadProducts(val, checkIfChecked);

}






$scope.addToList = function(val){

        var orderQuantity = 1 ;
        //var shoppingCartTypeId = $cookies.get('user_id') ;
        var productId =  val ;
        var shoppingCartTypeId = 1 ;





       var data = {productId:productId , shoppingCartTypeId:shoppingCartTypeId , quantity:orderQuantity};


        $http({
       method: 'post',
       data: data ,
       url: url+'/api/ShoppingCart/AddProductToCart?productId='+productId+'&shoppingCartTypeId='+shoppingCartTypeId+'&quantity='+orderQuantity,
       headers: {
        "Content-Type": "application/json",
        "Accept": 'application/json',
        'Authorization': token
        }
     }).then(function (response){

          $scope.message = $sce.trustAsHtml(response.data.message) ;
          $("#success_modal").modal('show');
          if(response.data.success == true){

               //$scope.message = $sce.trustAsHtml(response.data.message) ;
               $rootScope.$emit("GetCustomerReport", {});

          }


     });







};




});


//-------------------- END HERE ---------------------------//

app.controller('productDetailsController' , function($scope , $http , $sce , $cookies , $rootScope){

   var product_url_id = window.location.href.split('=')[1];
   var token = "Token "+$cookies.get('user_autheticate_details');



   var cookie_user_id = $cookies.get('user_id');


   var product_id = product_url_id ;
   if(product_url_id.indexOf("#") == -1){

        product_id = product_url_id ;

   }else{

       product_id = product_url_id.split("#")[0];

   }





$scope.getProductImage = function(){
 var data = {PageSize:10};
  $http({
       method: 'post',
     data: data,
     url: url+'/api/Products/'+product_id+'/ProductPictureList',

   }).then(function(response){


          $scope.product_picture = response.data.Data ;


   });







}

 $scope.getProductImage();

 $scope.increseCounter = function(event){

    var productCount = $("#orderQuantity").val();



    var newNumber = parseInt(productCount)+1 ;

    $("#orderQuantity").val(newNumber);


 }

 $scope.decreseCounter = function(event){

    var productCount = $("#orderQuantity").val();

    if(productCount == 0){

       return false;
    }

    var newNumber = parseInt(productCount)-1 ;

    $("#orderQuantity").val(newNumber);


 }

$scope.submitReview = function(){

var data =  {


    "AddProductReview": {
      "Title": $scope.review_title,
      "ReviewText": $scope.review_comment,
      "Rating": 5 - $(".glyphicon-star-empty").length,
      "CanCurrentCustomerLeaveReview": true,
      "SuccessfullyAdded": true,
      "Result": "sample string 6"
    }
  }

  $http({
       method: 'post',
       data: data,
     url: url+'/api/Products/Reviews/'+product_url_id+'/CreateReviews',
     headers: {
      "Content-Type": "application/json",
      "Accept": 'application/json',
      'Authorization': token
      }
   }).then(function (response){


            $scope.loadReview();

   });



}





   $scope.loadProductDetails = function(){




        $http({
             method: 'get',
           url: url+'/api/Products/'+product_url_id,
         }).then(function (response){
              console.log(response.data.OrderMaximumQuantity);
              response.data.html_short_description = $sce.trustAsHtml(response.data.ShortDescription);
              response.data.html_full_description = $sce.trustAsHtml(response.data.FullDescription);
               $scope.product_details = response.data  ;




                //  var review_data = {productId:product_url_id};

          });


   };



   $scope.loadReview = function(){

     var data = {PageSize:10};

        $scope.review_data  = [] ;

       $http({
          method: 'post',
          data: data,
          url: url+'/api/Products/Reviews/CustomerProductReviews/'+product_id,
          headers: {
           "Content-Type": "application/json",
           "Accept": 'application/json',
           'Authorization': token
           }

        }).then(function (response){
             console.log(response.data);
             $scope.total_review = response.data.Data.length ;
             $scope.review_data = response.data.Data ;





         }).catch(function(data){

             $scope.total_review = 0 ;
         });




   };

   $scope.loadReview();

   $scope.addToList = function(type){

           var orderQuantity = parseInt($("#orderQuantity").val());
           //var shoppingCartTypeId = $cookies.get('user_id') ;
           var productId = product_id ;

           if(type == "cart"){
           var shoppingCartTypeId = 1 ;
           }
           if(type == "wishlist"){

            var shoppingCartTypeId = 2 ;
           }



           var data = {productId:productId , shoppingCartTypeId:shoppingCartTypeId , quantity:orderQuantity};


           $http({
          method: 'post',
          data: data ,
          url: url+'/api/ShoppingCart/AddProductToCart?productId='+productId+'&shoppingCartTypeId='+shoppingCartTypeId+'&quantity='+orderQuantity,
          headers: {
           "Content-Type": "application/json",
           "Accept": 'application/json',
           'Authorization': token
           }
        }).then(function (response){

                 $scope.message = $sce.trustAsHtml(response.data.message) ;

             if(response.data.success == true){


                  $rootScope.$emit("GetCustomerReport", {});

             }


        });







   };



});


app.controller('registerController' , function($scope, $http){

  $scope.createCustomer = function(){






     var email = $scope.email ;



     var password = $scope.password ;

     var confirm_password = $scope.confirm_password ;

     var agree = $scope.agree ;





   //----------------- EMAIL VALIDATION GOES HERE ----------------//

   var reg = /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/ ;

   if(reg.test(email)){

       $scope.emailValidationerror = '';


   }else{

        $scope.emailValidationerror = 'Email is Invalid';
        return false;
   }






   //--------------- END HERE - -------------------------------------//



   if(typeof password == "undefined" || password == ""){


       $scope.passwordValidationError = 'Please Enter the Password';

       return false ;




   }

   else if(password.length < 6){

     $scope.passwordValidationError = 'Password length must be of 6 digits';

     return false ;



   }
   else{

      $scope.passwordValidationCheck = '' ;

   }




   //----------------------- END HERE ------------------------------------//


   if(password !== confirm_password){

      $scope.confirmPasswordValidationError = 'Password and Confirm Password doesn"t match' ;

      return false;



   }

   else{

       $scope.confirmPasswordValidationError = '';

   }



     //---- Agree Validation Check ------ //

     if(typeof agree == "undefined" || agree == false){

          $scope.message = "Please agree terms and condition" ;

          return false;
     }
     else{

         $scope.message = '';
     }




     //-----  MAKING POST REQUEST TO REGISTER A NEW CUSTOMER   ------ //

     $scope.status = "Registering ..." ;

      var data = {Email: email , Password: password , ConfirmPassword: confirm_password};


      $http({
           method: 'post',
           data: data,
         url: url+'/api/Account/Register',
       }).then(function (response){


          if(response.data.Success == false){

                   $scope.status = "This Account already exists" ;
                   $scope.color = "red" ;
              }
              else{
                $scope.status = "Thank you for Register. Please Login to continue" ;
                $scope.color = "green" ;

              }

                /*  if(typeof response.data.CustomerGuid != "undefined"){

                          $scope.status = 'Thank You for registering with us';

                  }else{


                       $scope.status = 'Oops Something went wrong' ;
                  }  */





       });



   }


});








app.controller('loginController' , function($scope , $http , $cookies){


      if(typeof $cookies.get('user_autheticate_details') != "undefined"){

           window.location.href = "/dreamsale" ;
      }



      $scope.makeLogin = function(){




         var email = $scope.login_email ;

         var password = $scope.login_password ;

         var remember = $scope.login_remeber ;


         if(typeof email == "undefined" || email == ""){

               $scope.login_message = "Please enter email" ;

               return false;

         }

         if(typeof password == "undefined" || password == ""){

           $scope.login_message = "Please enter Password" ;

           return false;
         }




  //------------------ POST RESPONSE HANDLE HERE ----------------------------- //
        var data = {Email: email , Password:password , RememberMe: remember};

        $http({
             method: 'post',
             data: data,
           url: url+'/api/Account/Login',
         }).then(function (response){

                if(typeof response.data.success != "undefined" && response.data.success == false){

                       $scope.login_message = "Sorry Invalid Credential" ;

                       return false;

                }else{

                   $cookies.put('user_autheticate_details' , response.data);
                   $cookies.put('logged_in_email' , email);
                    //alert($cookies.get('user_autheticate_details'));
                    window.location.href = "/dreamsale" ;
                }

              //  alert(response.data);

  });


//------------------- END HERE --------------------------------------------------//









      };





});



app.controller('ProfileController' , function($scope , $http , $cookies){

  $scope.logged_in_token = $cookies.get('user_autheticate_details');
  $scope.logged_in_email = $cookies.get('logged_in_email');

  var token = "Token "+$scope.logged_in_token ;
  $scope.Id = 0;
$scope.userRecord = function(){




  var data = {"SearchEmail": $scope.logged_in_email};



  $http({
    method: 'post',
    data: data ,
    url: url+'/api/customers/',
    headers: {
     "Content-Type": "application/json",
     "Accept": 'application/json',
     'Authorization': token
     }
  }).then(function (response){
        console.log(response.data);
        $scope.profile_data = response.data.Data[0] ;
        $scope.Id = response.data.Data[0].Id ;


  });

}

$scope.userRecord() ;



  $scope.updateUserRecord = function(){

      var id = $scope.Id ;

      var first_name = $("#first_name").val();

      var last_name = $("#last_name").val() ;

      var phonenumber = $("#phone_number").val() ;

      var email_address = $("#email_address").val() ;

      var street_address = $("#address").val() ;

      var city = $("#city").val();





      var data = {
          "Id": id,
          "Firstname": first_name,
          "Lastname":  last_name,
          "MobileNumber": phonenumber,
          "Email": email_address,
          "StreetAddress": street_address,
          "City": city,
          "CustomerRoleNames": "Registered"

     };



 $http({
       method: 'post',
       data: data ,
       url: url+'/api/customers/Update',
       headers: {
        "Content-Type": "application/json",
        "Accept": 'application/json',
        'Authorization': token
        }
     }).then(function (response){

          $scope.userRecord();


     });








  }




});



//------------------------ CART CONTROLLER --------------------------- //


//------------------ Index PRODUCT CONTROLLER ------------------------//




app.controller('indexController' , function($scope , $http , $cookies){
  $scope.logged_in_token = $cookies.get('user_autheticate_details');
  $scope.logged_in_email = $cookies.get('logged_in_email');

  var token = "Token "+$scope.logged_in_token ;

  var data = {"Page": 1 , "PageSize":4};
  $http({
    method: 'post',
    data: data ,
    url: url+'/api/Products/FeaturedProducts',

  }).then(function (response){
            console.log(response.data);
           $scope.featured_product = response.data ;

  });


  $scope.addToCart = function(val){

          var orderQuantity = 1 ;
          //var shoppingCartTypeId = $cookies.get('user_id') ;
          var productId = val ;
          var shoppingCartTypeId = 1 ;





          var data = {productId:productId , shoppingCartTypeId:shoppingCartTypeId , quantity:orderQuantity};


          $http({
         method: 'post',
         data: data ,
         url: url+'/api/ShoppingCart/AddProductToCart?productId='+productId+'&shoppingCartTypeId='+shoppingCartTypeId+'&quantity='+orderQuantity,
         headers: {
          "Content-Type": "application/json",
          "Accept": 'application/json',
          'Authorization': token
          }
       }).then(function (response){


            if(response.data.success == true){

                // $scope.message = $sce.trustAsHtml(response.data.message) ;
                 $rootScope.$emit("GetCustomerReport", {});

            }


       });







  };


});

//----------------------- END HERE ---------------------------------------------//


//------------------------ WISHLIST CONTROLLER --------------------------------//


app.controller('wishListController' , function($scope , $http , $cookies , $rootScope){


var token = "Token "+$cookies.get('user_autheticate_details');

$scope.getCartData = function(){





$http({
  method: 'get',

  url: url+'/api/ShoppingCart/GetCartDetailsLoggedinUser/2/',
  headers: {
   "Content-Type": "application/json",
   "Accept": 'application/json',
   'Authorization': token
   }
}).then(function (response){




          $scope.total_cart_length = response.data.Items.length ;
          $scope.cart_item = response.data.Items ;
   });

 }



   $scope.getCartData();


   $scope.removeItem = function(product_id){

     $http({
       method: 'post',

       url: url+'api/ShoppingCart/Delete/1/'+product_id,
       headers: {
        "Content-Type": "application/json",
        "Accept": 'application/json',
        'Authorization': token
        }
     }).then(function (response){


               $scope.getCartData();
               $rootScope.$emit("GetCustomerReport", {});


        });



   };



});





//------------------------- END HERE ----------------------------------------------//












//--=--------- CART PRODDUCT DATA --------------------------------//


app.controller('cartController' , function($scope , $http , $cookies , $rootScope){


var token = "Token "+$cookies.get('user_autheticate_details');

$scope.getCartData = function(){





$http({
  method: 'get',

  url: url+'/api/ShoppingCart/GetCartDetailsLoggedinUser/1/',
  headers: {
   "Content-Type": "application/json",
   "Accept": 'application/json',
   'Authorization': token
   }
}).then(function (response){

        //  alert(response.data);

          $scope.total_cart_length = response.data.Items.length ;
          $scope.cart_item = response.data;
   });

 }



   $scope.getCartData();


   $scope.removeItem = function(product_id){

     $http({
       method: 'post',

       url: url+'/api/ShoppingCart/Delete/1/'+product_id,
       headers: {
        "Content-Type": "application/json",
        "Accept": 'application/json',
        'Authorization': token
        }
     }).then(function (response){


               $scope.getCartData();
               $rootScope.$emit("GetCustomerReport", {});


        });



   };



});

//----------------------- END HERE ----------------------------------//


app.controller('cartCheckOutController' , function($scope, $http , $cookies){


    var token = "Token "+$cookies.get('user_autheticate_details');

    $scope.check_out_select = 0 ;

      $scope.getAllAddress = function(){

        $http({
          method: 'get',

          url: url+'/api/customers/'+$cookies.get('user_id')+'/Addresses',
          headers: {
           "Content-Type": "application/json",
           "Accept": 'application/json',
           'Authorization': token
           }
        }).then(function (response){


                 $scope.address_details = response.data.Data ;


           });

       $scope.getState = function(){

         $http({
           method: 'get',

           url: url+'/api/States',
           headers: {
            "Content-Type": "application/json",
            "Accept": 'application/json',
            'Authorization': token
            }
         }).then(function (response){


              $scope.stateList = response.data.Data ;

         });


       };

       $scope.getState();





      }

      $scope.getAllAddress() ;


      $scope.addaddress = function(){


        var address = {
  "Id": 1,
  "CustomerId": $cookies.get('user_id'),
  "Address": {
    "Id": 1,
    "CustomProperties": {
      "sample string 1": {},
      "sample string 3": {}
    },
    "FirstName": $scope.first_name,
    "LastName": $scope.last_name,
    "Email": $cookies.get('logged_in_email'),
    "Company": "sample string 5",
    "CountryId": 1,
    "CountryName": $scope.country,
    "StateProvinceId": 1,
    "StateProvinceName": $scope.states,
    "City":  $scope.city,
    "Address1": $scope.address_line_1,
    "Address2": $scope.address_line_2,
    "ZipPostalCode": $scope.postal_code ,
    "MobileNumber": $scope.mobile_number,
    "IsDefaultBillingAddress": true,
    "IsDefaultShippingAddress": true,
    "AddressHtml": "sample string 15",
    "FormattedCustomAddressAttributes": "sample string 16",
    "CustomAddressAttributes": [
      {
        "Id": 1,
        "Name": "sample string 2",
        "IsRequired": true,
        "DefaultValue": "sample string 4",
        "AttributeControlType": 1,
        "Values": [
          {
            "Id": 1,
            "Name": "sample string 2",
            "IsPreSelected": true
          },
          {
            "Id": 1,
            "Name": "sample string 2",
            "IsPreSelected": true
          }
        ]
      },
      {
        "Id": 1,
        "Name": "sample string 2",
        "IsRequired": true,
        "DefaultValue": "sample string 4",
        "AttributeControlType": 1,
        "Values": [
          {
            "Id": 1,
            "Name": "sample string 2",
            "IsPreSelected": true
          },
          {
            "Id": 1,
            "Name": "sample string 2",
            "IsPreSelected": true
          }
        ]
      }
    ],
    "AvailableCountries": [
      {
        "Disabled": true,
        "Group": {
          "Disabled": true,
          "Name": "sample string 2"
        },
        "Selected": true,
        "Text": "sample string 3",
        "Value": "sample string 4"
      },
      {
        "Disabled": true,
        "Group": {
          "Disabled": true,
          "Name": "sample string 2"
        },
        "Selected": true,
        "Text": "sample string 3",
        "Value": "sample string 4"
      }
    ],
    "AvailableStates": [
      {
        "Disabled": true,
        "Group": {
          "Disabled": true,
          "Name": "sample string 2"
        },
        "Selected": true,
        "Text": "sample string 3",
        "Value": "sample string 4"
      },
      {
        "Disabled": true,
        "Group": {
          "Disabled": true,
          "Name": "sample string 2"
        },
        "Selected": true,
        "Text": "sample string 3",
        "Value": "sample string 4"
      }
    ],
    "FirstNameEnabled": true,
    "FirstNameRequired": true,
    "LastNameEnabled": true,
    "LastNameRequired": true,
    "EmailEnabled": true,
    "EmailRequired": true,
    "CompanyEnabled": true,
    "CompanyRequired": true,
    "CountryEnabled": true,
    "CountryRequired": true,
    "StateProvinceEnabled": true,
    "CityEnabled": true,
    "CityRequired": true,
    "StreetAddressEnabled": true,
    "StreetAddressRequired": true,
    "StreetAddress2Enabled": true,
    "StreetAddress2Required": true,
    "ZipPostalCodeEnabled": true,
    "ZipPostalCodeRequired": true,
    "MobileNumberEnabled": true,
    "MobileNumberRequired": true
  }
}
        var data = {CustomerId:$cookies.get('user_id') , Address:address};

        $http({
          method: 'post',
          data: address,
          url: url+'/api/customers/CreateAddress',
          headers: {
           "Content-Type": "application/json",
           "Accept": 'application/json',
           'Authorization': token
           }
        }).then(function (response){

                  $("#myModal").modal('hide');

                  $scope.getAllAddress() ;



         });



      };


      $scope.deleteAddress = function(address_id){
        $http({
          method: 'post',

          url: url+'/api/customers/Address/Delete?addressId='+address_id,
          headers: {
           "Content-Type": "application/json",
           "Accept": 'application/json',
           'Authorization': token
           }
        }).then(function (response){

                       $scope.getAllAddress() ;


          });
      }


  $scope.editAddress = function(addressId){

    $http({
      method: 'get',

      url: url+'/api/customers/Address/'+addressId,
      headers: {
       "Content-Type": "application/json",
       "Accept": 'application/json',
       'Authorization': token
       }
    }).then(function (response){

          $scope.addressDetails = response.data ;

          $("#editModal").modal('show');

     });




  };



$scope.getOrderConfirmed = function(){

  $http({
    method: 'post',

    url: url+'/api/Checkout/ConfirmOrder',
    headers: {
     "Content-Type": "application/json",
     "Accept": 'application/json',
     'Authorization': token
     }
  }).then(function (response){

      //alert(response.data);
     if(typeof response.data.OrderId != "undefined"){

          $("#success_modal").modal('show');

          window.setTimeout(function(){

       // Move to a new location or you can do something else
       window.location.href = "myOrder.html";

     }, 1000);
     }

   });



}




  $scope.updateAddress = function(val){



        var address = {
      "Id": val,
      "CustomerId": $cookies.get('user_id'),
      "Address": {
        "Id": val,
        "CustomProperties": {
          "sample string 1": {},
          "sample string 3": {}
        },
        "FirstName": $("#first_name").val(),
        "LastName": $("#last_name").val(),
        "Email": $cookies.get('logged_in_email'),
        "Company": "sample string 5",
        "CountryId": 1,
        "CountryName": $("#country").val(),
        "StateProvinceId": 1,
        "StateProvinceName": $("#edit_state").val(),
        "City":  $("#edit_city").val(),
        "Address1": $("#address_line1").val(),
        "Address2": $("#address_line2").val(),
        "ZipPostalCode": $("#postal_code").val(),
        "MobileNumber": $("#mobile_number").val(),
        "IsDefaultBillingAddress": true,
        "IsDefaultShippingAddress": true,
        "AddressHtml": "sample string 15",
        "FormattedCustomAddressAttributes": "sample string 16",
        "CustomAddressAttributes": [
          {
            "Id": 1,
            "Name": "sample string 2",
            "IsRequired": true,
            "DefaultValue": "sample string 4",
            "AttributeControlType": 1,
            "Values": [
              {
                "Id": 1,
                "Name": "sample string 2",
                "IsPreSelected": true
              },
              {
                "Id": 1,
                "Name": "sample string 2",
                "IsPreSelected": true
              }
            ]
          },
          {
            "Id": 1,
            "Name": "sample string 2",
            "IsRequired": true,
            "DefaultValue": "sample string 4",
            "AttributeControlType": 1,
            "Values": [
              {
                "Id": 1,
                "Name": "sample string 2",
                "IsPreSelected": true
              },
              {
                "Id": 1,
                "Name": "sample string 2",
                "IsPreSelected": true
              }
            ]
          }
        ],
        "AvailableCountries": [
          {
            "Disabled": true,
            "Group": {
              "Disabled": true,
              "Name": "sample string 2"
            },
            "Selected": true,
            "Text": "sample string 3",
            "Value": "sample string 4"
          },
          {
            "Disabled": true,
            "Group": {
              "Disabled": true,
              "Name": "sample string 2"
            },
            "Selected": true,
            "Text": "sample string 3",
            "Value": "sample string 4"
          }
        ],
        "AvailableStates": [
          {
            "Disabled": true,
            "Group": {
              "Disabled": true,
              "Name": "sample string 2"
            },
            "Selected": true,
            "Text": "sample string 3",
            "Value": "sample string 4"
          },
          {
            "Disabled": true,
            "Group": {
              "Disabled": true,
              "Name": "sample string 2"
            },
            "Selected": true,
            "Text": "sample string 3",
            "Value": "sample string 4"
          }
        ],
        "FirstNameEnabled": true,
        "FirstNameRequired": true,
        "LastNameEnabled": true,
        "LastNameRequired": true,
        "EmailEnabled": true,
        "EmailRequired": true,
        "CompanyEnabled": true,
        "CompanyRequired": true,
        "CountryEnabled": true,
        "CountryRequired": true,
        "StateProvinceEnabled": true,
        "CityEnabled": true,
        "CityRequired": true,
        "StreetAddressEnabled": true,
        "StreetAddressRequired": true,
        "StreetAddress2Enabled": true,
        "StreetAddress2Required": true,
        "ZipPostalCodeEnabled": true,
        "ZipPostalCodeRequired": true,
        "MobileNumberEnabled": true,
        "MobileNumberRequired": true
      }
    };


    $http({
      method: 'post',
      data: address,
      url: url+'/api/customers/UpdateAddress',
      headers: {
       "Content-Type": "application/json",
       "Accept": 'application/json',
       'Authorization': token
       }
    }).then(function (response){

              $("#editModal").modal('hide');

              $scope.getAllAddress() ;



     });
}


 $scope.getCheckOutDetails = function(){


   $http({
     method: 'get',

     url: url+'/api/Checkout/',
     headers: {
      "Content-Type": "application/json",
      "Accept": 'application/json',
      'Authorization': token
      }
   }).then(function (response){


            //alert(response.data);


   });





 }

 $scope.getCheckOutDetails();


  $scope.selectAddress = function(event , val){

    $http({
      method: 'post',

      url: url+'/api/Checkout/SelectBillingAddress?addressId='+val+'&shipToSameAddress=1',
      headers: {
       "Content-Type": "application/json",
       "Accept": 'application/json',
       'Authorization': token
       }
    }).then(function (response){

            $scope.check_out_select = 1 ;


            var a = $(event.target).closest('a').html();

            $(".cehck_Adrress").html(a);
            $(event.target).html("Selected");

     });




  };


});



//---------------------- FETCHING DETAILS OF ORDER ----------------------------------------//


app.controller('orderController' , function($scope , $http , $cookies){
   var token = "Token "+$cookies.get('user_autheticate_details');


       $scope.getCustomerReport = function(){
         //alert(token);
      var data = {PageSize:10};
         $http({
           method: 'post',
           data: data,
           url: url+'/api/customers/'+  $cookies.get('user_id')+'/Orders',
           headers: {
            "Content-Type": "application/json",
            "Accept": 'application/json',
            'Authorization': token
            }
         }).then(function (response){

             $scope.order_details = response.data ;

          });


       };


         $scope.getCustomerReport();

});





//----------------- END HERE -----------------------------------------------------------//


//------------------------------  PROPERTY CONSULTANCY CONTROLLER --------------------//

app.controller('propertyonsultancyController' , function($scope , $http , $cookies){
      var token = "Token "+$cookies.get('user_autheticate_details');
    $scope.submitForm = function(){

       var residential = $scope.residential ;
       var sqft = $scope.sqft ;
       var budget = $scope.budget ;
       var scope_of_work = $scope.scope_of_work ;
       var name = $scope.name ;
       var email_address = $scope.email_address ;
       var phone_number = $scope.phone_number ;
       var alternate_phone = $scope.alternate_phone ;
       var city = $scope.city ;
       var pincode = $scope.pincode ;
       var comment = $scope.comment ;


         var data = {Subject: $scope.scope_of_work , body:comment , To:email_address};
          $http({
            method: 'post',
            data: data,
            url: url+'/api/EmailAccounts/SendEmail',
            headers: {
             "Content-Type": "application/json",
             "Accept": 'application/json',
             'Authorization': token
             }
          }).then(function (response){


                if(response.status == 200){
                    $scope.message = "Mail Successfully Sent";
                    $("#status_model").modal('show');
                }


          });





    }

});


//--------------------------- END HERE ------------------------------------------//

















String.prototype.trunc = String.prototype.trunc ||
function(n){

    // this will return a substring and
    // if its larger than 'n' then truncate and append '...' to the string and return it.
    // if its less than 'n' then return the 'string'
    return this.length>n ? this.substr(0,n-1)+'...' : this.toString();
};



app.filter('cut', function () {
        return function (value, wordwise, max, tail) {
            if (!value) return '';

            max = parseInt(max, 10);
            if (!max) return value;
            if (value.length <= max) return value;

            value = value.substr(0, max);
            if (wordwise) {
                var lastspace = value.lastIndexOf(' ');
                if (lastspace !== -1) {
                  //Also remove . and , so its gives a cleaner result.
                  if (value.charAt(lastspace-1) === '.' || value.charAt(lastspace-1) === ',') {
                    lastspace = lastspace - 1;
                  }
                  value = value.substr(0, lastspace);
                }
            }

            return value + (tail || ' …');
        };
    });



  //------------------------------------- AUTOCOMPLETE ANGULAR.JS -------------------------------------------------------------- //





app.directive('autoComplete', function($timeout) {
      return function(scope, iElement, iAttrs) {
              iElement.autocomplete({
                  source: scope[iAttrs.uiItems],
                  select: function() {
                      $timeout(function() {
                        iElement.trigger('input');
                      }, 0);
                  }
              });
      };
  });

















  //---------------------------------------- END HERE ------------------------------------------------------------------------------//




























   // end conreoller ==========================================================================
